<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IGV extends Model
{
	protected $fillable = ["Nombre","Apellido","Edad","Telefono","Mail","Numid","Urba_barrio","Casa_num","Edificio","Piso","Condominio","Dir_completa","Ciudad","Condado","Estado"];
}
