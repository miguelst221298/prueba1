<?php

namespace App\Http\Controllers;

use App\IGV;
use Illuminate\Http\Request;

class IGVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  iGV::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Inicio del test se guardan los datos basicos del encuestado

        $igv = iGV::create($request->all());
        $porcentaje =0;
        $I =0;
        $C=0;
        $D=0;
        $S=0;
        // Las variables del request deben venir con el nombre que tienen en el algoritmo ejemplo (ER,HR,...etc)
        //En el post deben venir los datos basicos seguido de toda la encuesta

        //Esta en estado restrictivo o cuarentena?
        $ER;
        if ($request->ER == 1){
            $ER=0;
        }else{
            $ER=1;
        }

        //Tiene horario restrictivo?
        $HR;
        if ($request->HR == 1){
            $HR=0;
        }else{
            $HR=1;
        }


        //9)Toda la información que usted brinda es responsable usted ante las autoridades correspondientes?
        $TINF;
        if ($request->TINF == 0){
            // Aqui termina la encuesta y se calcula el porcentaje solo con este puntaje
            $S = ($ER+$HR )/3;

            $porcentaje=((($I+$C+$D+$S)/4)*100);

            //Aqui se debe guardar en la segunda tabla el resultado y los demas datos
        }else{
            //Continua la encuesta dentro de este else 
            
            //Cual es su rango de edad
            $RE;
            if ($request->RE <=9){
                $RE=0.9;             }
            elseif ($request->RE <=20) {
               $RE=0.8;               }
            elseif ($request->RE <=30) {
               $RE=0.7;               }
            elseif ($request->RE <=40) {
               $RE=0.6;               }
            elseif ($request->RE <=50) {
               $RE=0.5;               }
            elseif ($request->RE <=60) {
               $RE=0.4;               }
            elseif ($request->RE <=70) {
               $RE=0.3;               }
            else{ $RE=0.2;}
            
            //Tiene enfermedades pre existentes?
            $EP;
            if ($request->EP ==1)
            {$RE=0.5;}
            else{$RE=1;}

            //12¿Conoce las precauciones de limpieza que debe tomar?
            $PL;
            if ($request->PL ==1)
            {$PL=1;}
            else{$PL=0;}

            //13)¿Usa tapabocas fuera de casa?
            $TB;
            if ($request->TB ==1)
            {$TB=1;}
            else{$TB=0;}

            //14)¿Realiza el lavado de manos frecuentemente?
            $LM;
            if ($request->LM ==1)
            {$LM=1;}
            else{$LM=0;}





            //Fin de la encuesta
        }

        return  $porcentaje;
    }


  

    /**
     * Display the specified resource.
     *
     * @param  \App\IGV  $iGV
     * @return \Illuminate\Http\Response
     */
    public function show(IGV $iGV)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IGV  $iGV
     * @return \Illuminate\Http\Response
     */
    public function edit(IGV $iGV)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IGV  $iGV
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IGV $iGV)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IGV  $iGV
     * @return \Illuminate\Http\Response
     */
    public function destroy(IGV $iGV)
    {
        //
    }
}
