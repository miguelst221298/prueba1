<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIGVSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_g_v_s', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('Nombre');
            $table->string('Apellido');
            $table->string('Edad');
            $table->string('Telefono');
            $table->string('Mail');
            $table->string('Numid');
            $table->string('Urba_barrio');
            $table->string('Casa_num');
            $table->string('Edificio');
            $table->string('Piso');
            $table->string('Condominio');
            $table->string('Dir_completa');
            $table->string('Ciudad');
            $table->string('Condado');
            $table->string('Estado');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_g_v_s');
    }
}
